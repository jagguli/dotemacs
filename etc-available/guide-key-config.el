(require 'guide-key)
(setq guide-key/guide-key-sequence '("C-x" "C-c"))
(guide-key-mode 1)  ; Enable guide-key-mode
(setq guide-key/highlight-command-regexp "org")
